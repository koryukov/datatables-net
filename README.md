# DataTables .NET

The [DataTables][jsDataTables] plug-in for the jQuery Javascript library 
is a powerfull and flexible tool for interaction controls to any HTML table. 
The plug-in provides server-side processing option that allows to handed off 
paging, searching and ordering actions to a server. 

The DataTables .NET is a .NET Framework class library that provides 
communication between the DataTables plug-in for jQuery Javascript library on 
client-side and a server-side ASP.NET application. The assembly defines members 
that are convertable from (to) JSON objects of the jQuery DataTables plug-in. 
The library uses Json.NET framework for serialization and deserialization.

See [documentation][dnDataTables]

[jsDataTables]: https://datatables.net/
[dnDataTables]: https://koryukov.gitlab.io/datatables-net