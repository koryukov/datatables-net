﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataTables;

namespace DataTables.Tests
{
    [TestClass]
    public class RequestTesting
    {
        [TestMethod]
        public void Creation()
        {
            var request = new Request();
            Assert.AreSame(null, request.Search, "Запрос должен быть создан с пстым параметром поиска.");
            Assert.AreSame(null, request.Order, "Запрос должен быть создан с пустым параметром сортировки.");
        }
    }
}
