﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DataTables
{
    /// <summary xml:lang="en">
    /// A server response that should be serialized to JSON expected by the DataTables plug-in on client side.
    /// </summary>
    /// <typeparam xml:lang="en" name="T">
    /// The type of the <see cref="DataTables.Response{T}.Data"/> array items. 
    /// The type should be serializedable into JSON.
    /// </typeparam>
    /// <seealso xml:lang="en" href="https://datatables.net/manual/server-side#Returned-data" target="_blank">DataTables returned data</seealso>
    /// <example xml:lang="en">
    /// <para>
    /// The following Web API  controller handles a POST request that DataTables makes to obtain server-side processing data. 
    /// The controller response contains collection of <c>MyClass</c> objects, total number of records before and after filtering has been applied. 
    /// If an error occurs then exception message is included to the response.
    /// </para>
    /// <code language="CSharp" source="Snippets\Examples.cs" region="Create Response"/>
    /// </example>
    public class Response<T>
    {
        /// <summary xml:lang="en">
        /// The draw counter that this object is a response to - from the <c>draw</c> parameter sent as part of the data request.
        /// </summary>
        /// <seealso cref="DataTables.Request.Draw"/>
        [JsonProperty(PropertyName = "draw")]
        public int Draw { get; set; }

        /// <summary xml:lang="en">
        /// The total number of records in the database.
        /// </summary>
        [JsonProperty(PropertyName = "recordsTotal")]
        public int Total { get; set; }

        /// <summary xml:lang="en">
        /// Total records, after filtering.
        /// </summary>
        /// <remarks xml:lang="en">
        /// The total number of records after filtering has been applied - not just the number of records being returned for this page of data.
        /// </remarks>
        [JsonProperty(PropertyName = "recordsFiltered")]
        public int Filtered { get; set; }

        /// <summary xml:lang="en">
        /// An array of data source objects, one for each row, which will be used by DataTables on client side to be displayed in the table.
        /// </summary>
        [JsonProperty(PropertyName = "data")]
        public T[] Data { get; set; }

        /// <summary xml:lang="en">
        /// Error message to be displayed when an error occurs during the running of the server-side processing script.
        /// </summary>
        /// <remarks xml:lang="en">
        /// <note type="important">Do not include if there is no error.</note>
        /// <para>If an error occurs during the running of the server-side processing script, 
        /// you can inform the user of this error by passing back the error message to be displayed using this parameter.</para>
        /// </remarks>
        [JsonProperty(PropertyName = "error", NullValueHandling = NullValueHandling.Ignore)]
        public string Error { get; set; }
    }
}
