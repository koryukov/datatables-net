﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTables
{
    /// <summary xml:lang="en">
    /// Object of an AJAX request, that is sended by the DataTables plug-in and converted into .NET equivalent.
    /// </summary>
    /// <example xml:lang="en">
    /// <para>
    /// The following method converts an instance of <see cref="Request"/> to an SQL <c>SELECT</c> statement. 
    /// The method result could be used with <see cref="System.Data.SqlClient.SqlCommand"/> to get 
    /// constrained number of the records from table <c>tableName</c>, 
    /// that are satisfy to search conditions and sorted in specified order.
    /// </para>
    /// <code language="CSharp" source="Snippets\Examples.cs" region="Request Conversion"/>
    /// </example>
    /// <seealso xml:lang="en" href="https://datatables.net/manual/server-side#Sent-parameters" target="_blank">
    /// Sent parameters of the DataTables
    /// </seealso>
    public class Request
    {
        /// <summary xml:lang="en">
        /// Draw counter, that is used to ensure, 
        /// that the AJAX returns from server-side processing requests are drawn in sequence by DataTables.
        /// </summary>
        /// <remarks xml:lang="en">AJAX requests are asynchronous and thus can return out of sequence.</remarks>
        /// <seealso cref="DataTables.Response{T}.Draw">Response&lt;T&gt;.Draw</seealso>
        public int Draw { get; set; }

        /// <summary xml:lang="en">
        /// Paging first record indicator.
        /// </summary>
        /// <remarks  xml:lang="en">
        /// This is the start point in the current data set (0 index based - i.e. 0 is the first record).
        /// </remarks>
        public int Start { get; set; }

        /// <summary xml:lang="en">
        /// Number of records that the table can display in the current draw. 
        /// It is expected that the number of records returned will be equal to this number, unless the server has fewer records to return. 
        /// </summary>
        /// <remarks xml:lang="en">
        /// This can be -1 to indicate that all records should be returned.
        /// </remarks>
        public int Length { get; set; }

        /// <summary xml:lang="en">
        /// Global search query parameters. 
        /// Be applied to all columns which have <see cref="DataTables.Column.Searchable"/> as <see langword="true"/>.
        /// </summary>
        public SearchParameters Search { get; set; }

        /// <summary xml:lang="en">
        /// Rows ordering parameters.
        /// </summary>
        /// <remarks xml:lang="en">
        /// Contains an array that defines how many columns are being ordered upon. 
        /// If the array length is 1, then a single column sort is being performed, otherwise a multi-column sort is being performed.
        /// </remarks>
        public OrderOptions[] Order { get; set; }

        /// <summary xml:lang="en">
        /// Defines all columns in the table.
        /// </summary>
        public Column[] Columns { get; set; }
    }

    /// <summary xml:lang="en">
    /// Provides query parameters to search rows.
    /// </summary>
    public class SearchParameters
    {
        /// <summary xml:lang="en">
        /// Search term value.
        /// </summary>
        public string Value { get; set; }

        /// <summary xml:lang="en">
        /// Flag to indicate if the search term <see cref="DataTables.SearchParameters.Value"/> should be treated as regular expression.
        /// </summary>
        /// <remarks xml:lang="en">
        /// Normally server-side processing scripts will not perform regular expression searching 
        /// for performance reasons on large data sets, but it is technically possible and at the discretion of your script.
        /// </remarks>
        public bool Regex { get; set; }
    }

    /// <summary xml:lang="en">
    /// Provides query parameters to order rows.
    /// </summary>
    public class OrderOptions
    {
        /// <summary xml:lang="en">
        /// Index of column to which ordering should be applied. 
        /// </summary>
        /// <remarks xml:lang="en">
        /// This is an index reference to the columns array of information that is also submitted to the server.
        /// </remarks>
        /// <seealso cref="DataTables.Request.Columns"/>
        public int Column { get; set; }

        /// <summary xml:lang="en">
        /// Ordering direction for the column specified by <see cref="OrderOptions.Column"/>.
        /// </summary>
        public Direction Dir { get; set; }

        /// <summary xml:lang="en">
        /// Provides multiple of ordering direction values.
        /// </summary>
        public enum Direction
        {
            /// <summary xml:lang="en">
            /// Ascending ordering.
            /// </summary>
            Asc,

            /// <summary xml:lang="en">
            /// Descending ordering.
            /// </summary>
            Desc
        }
    }

    /// <summary xml:lang="en">
    /// Provides metadata and server-side processing parameters for DataTables column.
    /// </summary>
    public class Column
    {
        /// <summary xml:lang="en">
        /// Name of the dataset field that is source for the column's data.
        /// </summary>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/columns.data" target="_blank">columns.data option</seealso>
        public string Data { get; set; }

        /// <summary xml:lang="en">
        /// Column's name that is used to be able to address individual columns.
        /// </summary>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/columns.name" target="_blank">columns.name option</seealso>
        public string Name { get; set; }

        /// <summary xml:lang="en">
        /// Flag to indicate whether this column is searchable or not.
        /// </summary>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/columns.searchable" target="_blank">
        /// columns.searchable option
        /// </seealso>
        public bool Searchable { get; set; }

        /// <summary xml:lang="en">
        /// Flag to indicate whether this column is orderable or not.
        /// </summary>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/columns.orderable" target="_blank">
        /// columns.orderable option
        /// </seealso>
        public bool Orderable { get; set; }

        /// <summary xml:lang="en">
        /// Search parameters to apply to this specific column.
        /// </summary>
        public SearchParameters Search { get; set; }
    }
}
